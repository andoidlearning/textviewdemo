package com.example.textviewdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnCnt : Button = findViewById(R.id.btn_count)
        val textView : TextView = findViewById(R.id.textView)

        var count = 0

        btnCnt.setOnClickListener {
            count++
            textView.text = "Let's Count : $count"
        }
    }
}